# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian-wheezy"

  boxes = {
    :puppet => {
      :hostname       => "puppet.dhoppe.lan",
      :ip             => "192.168.56.10",
      :cpus           => "2",
      :memory         => "512",
      :forwarded_port => { 80 => 10080, 443 => 10443 },
    },
    :pclient1 => {
      :box            => "debian-squeeze",
      :hostname       => "pclient1.dhoppe.lan",
      :ip             => "192.168.56.11",
    },
    :pclient2 => {
      :hostname       => "pclient2.dhoppe.lan",
      :ip             => "192.168.56.12",
    },
    :pclient3 => {
      :box            => "ubuntu-precise",
      :hostname       => "pclient3.dhoppe.lan",
      :ip             => "192.168.56.13",
    },
    :pclient4 => {
      :box            => "ubuntu-raring",
      :hostname       => "pclient4.dhoppe.lan",
      :ip             => "192.168.56.14",
    },
  }

  boxes.each do |name,setup|
    config.vm.define name do |vm_config|
      vm_config.vm.box = setup[:box] if setup[:box]
      vm_config.vm.hostname = setup[:hostname] if setup[:hostname]
      vm_config.vm.network :private_network, ip: setup[:ip] if setup[:ip]

      config.vm.provider :virtualbox do |vm_provider|
        unless setup[:cpus]
          vm_provider.customize ["modifyvm", :id, "--cpus", "1"]
        else 
          vm_provider.customize ["modifyvm", :id, "--cpus", setup[:cpus]]
        end

        unless setup[:memory]
          vm_provider.customize ["modifyvm", :id, "--memory", "256"]
        else 
          vm_provider.customize ["modifyvm", :id, "--memory", setup[:memory]]
        end
      end

      if setup[:forwarded_port]
        setup[:forwarded_port].each do |guest,host|
          vm_config.vm.network :forwarded_port, guest: guest, host: host
        end
      end
    end
  end
end